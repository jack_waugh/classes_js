// Author: Jack Waugh
// License: MIT ("Expat" version).
// Published at: https://bitbucket.org/jack_waugh/classes_js
// Even if this file or a copy of it is found within a project that is by and
// large a work made for hire, then this file or its copy is not part of the
// work made for hire. It is still owned by me, Jack Waugh.

// Snippet for starting over: delete com.jackwaugh

( function () {

  var assure_namespace_in__at = function (base, k) {
    var r;
    if ('object' != typeof (r = base[k])) base[k] = r = {};
    return r
  };

  var com = assure_namespace_in__at(window, 'com');
  var support = assure_namespace_in__at(com, 'jackwaugh');
  support.assure_namespace_in__at = assure_namespace_in__at;

  support.assure_namespace_at_global_path = function () {
    var cur, x, lim;
    lim = arguments.length;
    cur = window;
    for (x = 0; x < lim; x++)
      cur = this.assure_namespace_in__at(cur, arguments[x]);
    return cur
  };

  /*
    Varible slots proposed for a class:
      - instance_methods (combined public and private);
      - public_instance_methods;
      - Variables, a constructor function to make a JS object for each
        instance's variables.
  */
  var class_methods = {
    add_private_method: function (k, v) {
      this._instance_methods[k] = v
    },
    add_public_method: function (k, v) {
      this.       _instance_methods[k] = v;
      this._public_instance_methods[k] = v
    },
    add_class_method: function (k, v) {
      this[k] = v
    },
    debugging: function () {
      switch (arguments.length) {
      case 0: return this._debugging;
      case 1: return this._debugging = arguments[0];
      default: throw "Need 0 or 1 arg, not more count."
      } 
    },
    new_variables: function () {
      return new this.Variables()
    },
    shims_for_variables: function (vars) {
      // This returns a JS object full of shims, and this object is what we
      // publish as the identity of the instance, which consists of two JS
      // objects. Outsiders call this object and so see only the public methods.
      // The "bind" below causes the movement of control from the outside to
      // the inside context, where variables and private methods become
      // available to the code.
      var r = {};
      var pub = this._public_instance_methods;
      for (k in pub) r[k] = pub[k].bind(vars);
      if (this._debugging) {
        r._vars = vars;
        r._class = this;
      };
      return r
    },
    add_constructor: function (k, v) {
      this[k] = function () {
        var vars = this.new_variables();
        v.apply(vars, arguments);
        return this.shims_for_variables(vars)
      }
    },
    new: function () {
      var vars = this.new_variables();
      if ('function' === typeof this._instance_methods.initialize)
        vars.initialize.apply(vars, arguments);
      return this.shims_for_variables(vars)
    },
    add_setter_getter: function (k) {
      var pvt_name = '_' + k; // Closed over.
      this.add_public_method(k, function () {
        switch (arguments.length) {
        case 0: return this[pvt_name];
        case 1: return this[pvt_name] = arguments[0];
        default: throw "Need exactly 0 or 1 arg."
        }
      })
    },
  };
  var Class = function () {
    this.       _instance_methods = {};
    this._public_instance_methods = {};
    this.Variables = function () {};
    this.Variables.prototype = this._instance_methods;
  };
  Class.prototype = class_methods;
  support.Class = Class;
  support.new_class = function () {return new this.Class()};

  // Test private methods.
  ( function () {
    var Thing = support.new_class();
    Thing.add_private_method('prv', function () {return 'prv called.'});
    Thing.add_public_method( 'pub', function () {return this.prv()});
    var thing = Thing.new();
    if (undefined     !== thing.prv ) throw "Test failed."
    if ("prv called." != thing.pub()) throw "Test failed."
  } )();

  // Test setter/getter feature (like Ruby attr_accessor).
  ( function () {
    var Thing = support.new_class();
    Thing.debugging(true);
    Thing.add_setter_getter('myvar');
    var thing = Thing.new();
    thing.myvar(4);
    var other_thing = Thing.new();
    other_thing.myvar(5);
    if (4 != thing.myvar()) throw "Test failed."
  } )();

  // Test that a public method can call another public method.
  ( function () {
    var Thing = support.new_class();
    Thing.debugging(true);
    Thing.add_public_method('final', function () {return "final"});
    Thing.add_public_method('initial', function () { return this.final() });
    var thing = Thing.new();
    if ("final" != thing.initial()) throw "Test failed."
  } )();
})()
