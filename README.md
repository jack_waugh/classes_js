# classes_js

Support for Javascript (js) programming, including the ability to create classes with private methods.

# Installation

Currently this has been tested only in the browser environment. Some rework will be required in order
to support nodejs. To install for browser use, just copy the one source file into your project.

# Future

Currently there is no support for subclassing, as I have not as yet come across a need for it. But I
think that adding subclassing when it is needed should be fairly straightforward. Maybe.

# License

MIT ("Expat" version) [https://en.wikipedia.org/wiki/MIT_License]

# Usage

For examples, see the test cases near the end of the source file.

When creating a class, you ask the support system for a new class. You then call
the new class to add each public or private instance method you want it to
support. There is an option to add public access methods for instance variables
as well. Once you have added any of these things you want to your class, you
can then call the class to ask it for a new instance of itself.

# Terminology

Javascript jargon abuses the term "prototype" to mean the parent of an object.
Here, I use the term "parent" for this concept in Javascript, or I might
just say a Javascript object "inherits from" another Javascript object.

Javascript is a "prototyping" object-oriented language. The pioneering
example of a "prototyping" object-oriented language as distinct from a
class-based object-oriented programminng language is the "Self" language.
Typical usage of Self involves creating an object that is intended to be
shallow-copied for creating the working objects that take the place of class
instances in the typical usage of class-based languages. The object that is
intended to be cloned for this purpose is called the prototype. This kind of
use is the basis for referring to OO languages that do not have classes as a
fundamental built-in concept, "prototyping languages". In Self, an object
can have multiple parent objects via slots marked as inheriting. In Javascript,
an object can have at most one parent object, and this parent object is
erroneously referred to in Javascript's typical jargon as the "prototype",
and this erroneous jargon makes it into the names used in Javasript's
metaprogramming protocol. But just because vast numbers of people make a
lingual mistake and bring it into standards as well, is no reason that I
should participate in the mistake by incorporating it into documentation
such as what you are reading now.

# How It Works

A class has Javascript objects containing the public methods and all the
methods to be shared by the instances.

When you ask the class for a new instance, the class whips up two Javascript
objects with which to implement the instance. One of these objects holds
the varables for the instance and inherits from the single Javascript object
(belonging to the class) that holds the instance methods.

The other Javascript object that helps make up the instance, holds only shims
for the public instance methods. Each shim is a binding of an instance method
such that when the binding is called, the method is invoked, but the 'this' for
the invocation points to the object holding the variables and inheriting from
the object of instance methods, both public and private. It is the object of
shims that gets returned as the public identity of the instance.

To call a public method of an instance, you use normal Javascript calling with
the dot syntax, e. g. thing.do_something(args...). These calls only
see the public methods, as they are the only methods for which shims are
provided.

# A Path Not Taken

It would be possible to implement a class system in which instances would
be published as functions, and a call on an instance would have the form
thing('do_something', args...). It might be possible to conceive of some
advantages to such an approach as compared to the approach I take in the
current work. However, on balance, I decided to stick with the dot syntax
for readability, once I figured out a way to make it work and hide the
private methods and private variables. But if you want to try the other
approach, I would be interested to know how it turns out. Note that
jQuery is called that way.
